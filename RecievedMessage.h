#pragma once

#include "User.h"

class RecievedMessage
{
public:
	RecievedMessage(SOCKET sock, int messageCode);
	RecievedMessage(SOCKET sock, int messageCode, vector<string> values);

	//getters
	SOCKET getSock();
	User* getUser();
	int getMessageCode();
	vector<string>& getValues();

	//setters
	void setUser(User* user);

private:
	SOCKET _sock;
	User* _user;
	int _messageCode;
	vector<string> _values;
};